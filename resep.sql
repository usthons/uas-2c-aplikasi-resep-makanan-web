/*
SQLyog Ultimate v12.5.1 (64 bit)
MySQL - 10.4.6-MariaDB : Database - resep
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`resep` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `resep`;

/*Table structure for table `alat` */

DROP TABLE IF EXISTS `alat`;

CREATE TABLE `alat` (
  `kode_alat` varchar(11) NOT NULL,
  `nama_alat` varchar(50) DEFAULT NULL,
  `photo` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`kode_alat`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `alat` */

insert  into `alat`(`kode_alat`,`nama_alat`,`photo`) values 
('0','Wajan','wajan.jpg'),
('AL002','Spatula','spatula.jpg'),
('AL003','Telenan','telenan.jpg'),
('AL004','Pisau','pisau.jpg'),
('AL005','Pisau Daging','pisaudaging.jpg'),
('AL006','Baskom','baskom.jpg'),
('AL007','Panci','panci.jpg'),
('AL008','Ulekan','ulekan.jpg');

/*Table structure for table `kategori` */

DROP TABLE IF EXISTS `kategori`;

CREATE TABLE `kategori` (
  `id_kat` int(11) NOT NULL AUTO_INCREMENT,
  `nama_kat` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id_kat`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `kategori` */

insert  into `kategori`(`id_kat`,`nama_kat`) values 
(1,'Healty'),
(2,'Junk'),
(3,'Fresh');

/*Table structure for table `resep` */

DROP TABLE IF EXISTS `resep`;

CREATE TABLE `resep` (
  `kode` varchar(4) NOT NULL,
  `nama` varchar(50) DEFAULT NULL,
  `bahan1` varchar(100) DEFAULT NULL,
  `bahan2` varchar(100) DEFAULT NULL,
  `bahan3` varchar(100) DEFAULT NULL,
  `id_kat` int(11) DEFAULT NULL,
  `photo` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`kode`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `resep` */

insert  into `resep`(`kode`,`nama`,`bahan1`,`bahan2`,`bahan3`,`id_kat`,`photo`) values 
('M001','SOP JAGUNG','Jagung Manis','Telur','Kacang Polong',1,'sopjagung.jpg');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
