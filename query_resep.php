<?php

$DB_NAME = "resep";
$DB_USER = "root";
$DB_PASS = "";
$DB_SERVER_LOC = "localhost";

if($_SERVER['REQUEST_METHOD'] == 'POST'){
    $conn = mysqli_connect($DB_SERVER_LOC,$DB_USER,$DB_PASS,$DB_NAME);
    $mode = $_POST['mode'];
    $respon = array(); $respon['kode'] = '000';
    switch($mode){
        case "insert":
            $kode = $_POST['kode'];
            $nama = $_POST['nama'];
            $bahan1 = $_POST['bahan1'];
            $bahan2 = $_POST['bahan2'];
            $bahan3 = $_POST['bahan3'];
            $nama_kat = $_POST['nama_kat'];
            $imstr = $_POST['image'];
            $file = $_POST['file'];
            $path = "images/";

            $sql = "select id_kat from kategori where nama_kat = '$nama_kat'";
            $result = mysqli_query($conn, $sql);
            if(mysqli_num_rows($result)>0){
                $data = mysqli_fetch_assoc($result);
                $id_kat = $data['id_kat'];

                $sql = "insert into resep(kode, nama, bahan1, bahan2, bahan3, id_kat, photo) values(
                    '$kode','$nama','$bahan1','$bahan2','$bahan3','$id_kat','$file'
                )";
                $result = mysqli_query($conn,$sql);
                if($result){
                    if(file_put_contents($path.$file,base64_decode($imstr)) == false){
                        $sql = "delete from resep where kode = '$kode'";
                        mysqli_query($conn,$sql);
                        $respon['kode'] = "111";
                        echo json_encode($respon); exit();
                    }else{
                        echo json_encode($respon); exit();
                    }
                }else{
                    $respon['kode'] = "111";
                    echo json_encode($respon); exit();
                }
            }
        break;
        case "update":
            $kode = $_POST['kode'];
            $nama = $_POST['nama'];
            $bahan1 = $_POST['bahan1'];
            $bahan2 = $_POST['bahan2'];
            $bahan3 = $_POST['bahan3'];
            $nama_kat = $_POST['nama_kat'];
            $imstr = $_POST['image'];
            $file = $_POST['file'];
            $path = "images/";

            $sql = "select id_kat from kategori where nama_kat = '$nama_kat'";
            $result = mysqli_query($conn, $sql);

            if(mysqli_num_rows($result)>0){
                $data = mysqli_fetch_assoc($result);
                $id_kat= $data['id_kat'];

                $sql = "";
                if($imstr == ""){
                    $sql = "update resep set nama='$nama', bahan1='$bahan1', bahan2='$bahan2', bahan3='$bahan3',id_kat='$id_kat' where kode='$kode'";
                    $result = mysqli_query($conn,$sql);
                    if($result){
                        echo json_encode($respon); exit();
                    }else{
                        $respon['kode'] = "111";
                        echo json_encode($respon); exit();
                    }
                }else{
                    if(file_put_contents($path.$file,base64_decode($imstr)) == false){
                        $respon['kode'] = "111";
                        echo json_encode($respon); exit();
                    }else{
                        $sql = "update resep set nama='$nama', bahan1='$bahan1', bahan2='$bahan2', bahan3='$bahan3',id_kat='$id_kat', photo='$file' where kode='$kode'";
                        $result = mysqli_query($conn,$sql);
                        if($result){
                            echo json_encode($respon); exit();
                        }else{
                            $respon['kode'] = "111";
                            echo json_encode($respon); exit();
                        }
                    }
                }
            }
        break;
        case "delete":
            $kode = $_POST['kode'];
            $sql = "select photo from resep where kode = '$kode'";
            $result = mysqli_query($conn,$sql);
            if($result){
                if(mysqli_num_rows($result)>0){
                    $data = mysqli_fetch_assoc($result);
                    $photo = $data['photo'];
                    $path = "images/";
                    unlink($path.$photo);
                }
                $sql = "delete from resep where kode = '$kode'";
                $result = mysqli_query($conn,$sql);
                if($result){
                    echo json_encode($respon); exit();
                }else{
                    $respon['kode'] = '111';
                    echo json_encode($respon); exit();
                }
            }
        break;
    }
}