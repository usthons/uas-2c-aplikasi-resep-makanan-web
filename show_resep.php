<?php 

    $DB_NAME = "resep";
    $DB_USER = "root";
    $DB_PASS = "";
    $DB_SERVER_LOC = "localhost";

    if($_SERVER['REQUEST_METHOD'] == 'POST'){
        $conn = mysqli_connect($DB_SERVER_LOC,$DB_USER,$DB_PASS,$DB_NAME);
        $nama = $_POST['nama'];
        $sql = "SELECT r.kode, r.nama, r.bahan1, r.bahan2, r.bahan3, k.nama_kat, concat('http://192.168.43.62/resep/images/',r.photo) as url
            FROM resep r, kategori k 
            WHERE r.id_kat = k.id_kat
            and r.nama like '%$nama%'";
        $result = mysqli_query($conn,$sql);
        if(mysqli_num_rows($result) > 0){
            header("Access-Control-Allow-Origin: *");
            header("Content-type: application/json; charset=UTF-8");

            $data_resep = array();
            while($resep = mysqli_fetch_assoc($result)){
                array_push($data_resep,$resep);
            }
            echo json_encode($data_resep);
        }
    }