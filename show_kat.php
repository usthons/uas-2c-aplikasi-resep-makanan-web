<?php 

    $DB_NAME = "resep";
    $DB_USER = "root";
    $DB_PASS = "";
    $DB_SERVER_LOC = "localhost";

    if($_SERVER['REQUEST_METHOD'] == 'POST'){
        $conn = mysqli_connect($DB_SERVER_LOC,$DB_USER,$DB_PASS,$DB_NAME);
        $nama_kat = $_POST['nama_kat'];
        if(empty($nama_kat)){
            $sql = "select * from kategori order by nama_kat asc";
        }else{
            $sql = "select * from kategori where nama_kat like '%$nama_kat%' order by nama_kat asc";
        }

        $result = mysqli_query($conn,$sql);
        if(mysqli_num_rows($result) > 0){
            header("Access-Control-Allow-Origin: *");
            header("Content-type: application/json; charset=UTF-8");

            $nama_kat = array();
            while($kat = mysqli_fetch_assoc($result)){
                array_push($nama_kat,$kat);
            }
            echo json_encode($nama_kat);
        }
    }