<?php 

    $DB_NAME = "resep";
    $DB_USER = "root";
    $DB_PASS = "";
    $DB_SERVER_LOC = "localhost";

    if($_SERVER['REQUEST_METHOD'] == 'POST'){
        $conn = mysqli_connect($DB_SERVER_LOC,$DB_USER,$DB_PASS,$DB_NAME);
        $namaalat = $_POST['nama_alat'];
        $sql = "SELECT a.kode_alat, a.nama_alat, concat('http://192.168.43.62/resep/imagesalat/',a.photo) as url
            FROM alat a WHERE nama_alat like '%$namaalat%'";

        $result = mysqli_query($conn,$sql);
        if(mysqli_num_rows($result) > 0){
            header("Access-Control-Allow-Origin: *");
            header("Content-type: application/json; charset=UTF-8");

            $data_alat = array();
            while($alat = mysqli_fetch_assoc($result)){
                array_push($data_alat,$alat);
            }
            echo json_encode($data_alat);
        }
    }