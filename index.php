<?php
    $DB_NAME = "resep";
    $DB_USER = "root";
    $DB_PASS = "";
    $DB_SERVER_LOC = "localhost";

    $conn = mysqli_connect($DB_SERVER_LOC,$DB_USER,$DB_PASS,$DB_NAME);
    $sql = "SELECT r.kode, r.nama, r.bahan1, r.bahan2, r.bahan3, k.nama_kat, r.photo
            FROM resep r, kategori k 
            WHERE r.id_kat = k.id_kat";
    
    $result = mysqli_query($conn,$sql);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Data Mahasiswa</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
</head>
<body class="bg-light">
    <div class="container">
        <div class="row justify-content-center mt-5">
            <h4>Data Resep</h4>
            <table class="mt-3 table table-stripped">
                <tr>
                    <th>KODE</th>
                    <th>Nama</th>
                    <th>Bahan 1</th>
                    <th>Bahan 2</th>
                    <th>Bahan 3</th>
                    <th>Kategori</th>
                    <th>Foto</th>
                </tr>
                <?php 
                    while($resep = mysqli_fetch_assoc($result)){
                ?>
                <tr>
                <td><?php echo $resep['kode']; ?></td>
                    <td><?php echo $resep['nama']; ?></td>
                    <td><?php echo $resep['bahan1']; ?></td>
                    <td><?php echo $resep['bahan2']; ?></td>
                    <td><?php echo $resep['bahan3']; ?></td>
                    <td><?php echo $resep['nama_kat']; ?></td>
                    <td><img src="images/<?php echo $resep['photo']; ?>" style="width: 100px;" alt="Foto Makanan"></td>
                </tr> 
                <?php } ?>
            </table>
        </div>
    </div>
</body>
</html>